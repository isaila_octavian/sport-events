package com.example.textdemo;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
@RunWith(AndroidJUnit4.class)
public class LoginTest {
    FirebaseAuth fAuth;
    String userID;

    @Test
    public void useWrongPass() {
        fAuth = FirebaseAuth.getInstance();
        String email = "isaila_marian@yahoo.com";
        String password = "Mariann.1";
        fAuth.createUserWithEmailAndPassword(email,password);

        fAuth.signInWithEmailAndPassword(email, "Mariann.11").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                assertEquals(task.isSuccessful(), false);
            }
        });
    }

    @Test
    public void emailVerification() {
        fAuth = FirebaseAuth.getInstance();
        String email = "isaila_octavian98@yahoo.com";
        String password = "Octaviann.1";
        fAuth.createUserWithEmailAndPassword(email,password);

        fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    FirebaseUser user = fAuth.getCurrentUser();
                    assertEquals(false, user.isEmailVerified());
                }
            }
        });
    }

    @Test
    public void successfullyLogin() {
        fAuth = FirebaseAuth.getInstance();
        String email = "isaila_octavian98@yahoo.com";
        String password = "Octaviann.1";
        fAuth.createUserWithEmailAndPassword(email,password);

        fAuth.signInWithEmailAndPassword(email,password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                FirebaseUser user = authResult.getUser();
                assertNotEquals(null,user);
            }
        });
    }

}