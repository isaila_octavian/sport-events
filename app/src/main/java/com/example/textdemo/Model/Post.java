package com.example.textdemo.Model;

import java.util.Date;

public class Post extends PostId{

    private String image, user, description;
    private Date time;

    public String getImage() {
        return image;
    }

    public String getUser() {
        return user;
    }

    public String getDescription() {
        return description;
    }

    public Date getTime() {
        return time;
    }
}
