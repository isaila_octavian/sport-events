package com.example.textdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class QrGenerator extends AppCompatActivity {
    ImageView ivOutput;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_generator);

        getSupportActionBar().setTitle("User QR Code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ivOutput = findViewById(R.id.iv_output);

        fStore = FirebaseFirestore.getInstance();
        fAuth = FirebaseAuth.getInstance();


        userId = fAuth.getCurrentUser().getUid();
        DocumentReference documentReference = fStore.collection("users").document(userId);
        Map<String,Object> user = new HashMap<>();
        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                String buletinUser = documentSnapshot.getString("buletinUser");
                buletinUser = buletinUser.replaceAll("ă", "&a");
                buletinUser = buletinUser.replaceAll("â", "#a");
                buletinUser = buletinUser.replaceAll("î", "&i");
                buletinUser = buletinUser.replaceAll("ș", "&s");
                buletinUser = buletinUser.replaceAll("ț", "&t");
                buletinUser = buletinUser.replaceAll("Ă", "&A");
                buletinUser = buletinUser.replaceAll("Â", "#A");
                buletinUser = buletinUser.replaceAll("Î", "&I");
                buletinUser = buletinUser.replaceAll("Ș", "&S");
                buletinUser = buletinUser.replaceAll("Ț", "&T");
                System.out.println("Qr code " + buletinUser);
                //Initialize multi format writer
                MultiFormatWriter writer = new MultiFormatWriter();
                try {
                    //byte[] arrByteForSpanish = buletinUser.getBytes("windows-1250");

                    //String strSpanish = new String(arrByteForSpanish);
                    //Initialize bit matrix
                    BitMatrix matrix = writer.encode(buletinUser, BarcodeFormat.QR_CODE, 350,350);
                    //Initialize BarCode encoder
                    BarcodeEncoder encoder = new BarcodeEncoder();
                    //Initialize bitmap
                    Bitmap bitmap = encoder.createBitmap(matrix);
                    //set bitmap on imageView
                    ivOutput.setImageBitmap(bitmap);
                } catch (WriterException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        try {
            switch (item.getItemId()) {
                case android.R.id.home:
                    startActivity(new Intent(getApplicationContext(), Profile.class));
                    finish();
                    return true;
            }
        } catch (Exception e) {
            Toast.makeText(QrGenerator.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void profileComponent(View view) {
        startActivity(new Intent(getApplicationContext(), Profile.class));
        finish();
    }
}