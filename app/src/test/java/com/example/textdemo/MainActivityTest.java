package com.example.textdemo;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTest {
    String[][] arr = {
            {"01","AB", "AX", "Alba"},
            {"02","AR","AR","Arad"},
            {"03","AG","AS","AZ","Argeş"},
            {"04","BC","XC","ZC","Bacău"},
            {"05","BH","XH","ZH","Bihor"},
            {"06","BN","XB","Bistriţa-Năsăud"},
            {"07","BT","XT","Botoşani"},
            {"09","BR","XR","Brăila"},
            {"08","BV","BV","ZV","Braşov"},
            {"40","41","42","43","44","45","46","B","DP","DR","DT","DX","RD","RR","RT","RX","RK","Bucureşti"},
            {"10","BZ","XZ","Buzău"},
            {"51","CL","KL","Călăraşi"},
            {"11","CS","KS","Caraş-Severin"},
            {"12","CJ","KX","CJ","Cluj"},
            {"13","CT","KT","KZ","Constanţa"},
            {"14","CV","KV","Covasna"},
            {"15","DB","DD","Dâmboviţa"},
            {"16","DJ","DX","DZ","Dolj"},
            {"17","GL","GL","ZL","Galaţi"},
            {"52","GR","GG","Giurgiu"},
            {"18","GJ","GZ","Gorj"},
            {"19","HR","HR","Harghita"},
            {"20","HD","HD","Hunedoara"},
            {"21","IL","SZ","Ialomiţa"},
            {"22","IS","MX","MZ","Iaşi"},
            {"23","IF","IF","Ilfov"},
            {"24","MM","MX","MZ","Maramureş"},
            {"25","MH","MH","Mehedinţi"},
            {"26","MS","ZS","MS","Mureş"},
            {"27","NT","NT","Neamţ"},
            {"28","OT","OT","Olt"},
            {"29","PH","PH","PX","Prahova"},
            {"31","SJ","SX","Sălaj"},
            {"30","SM","SM","Satu Mare"},
            {"32","SB","SB","Sibiu"},
            {"33","SV","SV","XV","Suceava"},
            {"34","TR","TR","Teleorman"},
            {"35","TM","TM","TZ","Timiş"},
            {"36","TL","TC","Tulcea"},
            {"38","VL","VX","Vâlcea"},
            {"37","VS","VS","Vaslui"},
            {"39","VN","VN","Vrancea"}
    };

    @Test
    public void judet_isCorrect() {
        String judet = "Alba";
        boolean este_judet = MainActivity.validareJudet(judet, arr);
        assertEquals(true, este_judet);
    }

    @Test
    public void serie_isCorrect() {
        String serie = "KX";
        String judet = "Cluj";
        boolean este_serie = MainActivity.validareSerie(serie,judet,arr);
        assertEquals(true, este_serie);
    }

    @Test
    public void cnp_isCorrect() {
        String cnp = "1980501011849";
        String verificat = MainActivity.validareCNP(cnp);
        assertEquals("", verificat);
    }

}